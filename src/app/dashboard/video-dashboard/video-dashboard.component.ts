import { Component, OnInit, OnDestroy } from '@angular/core';
import { Observable, Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';

import { Video } from '../../types';
import { VideoLoaderService } from '../../video-loader.service';

@Component({
  selector: 'app-video-dashboard',
  templateUrl: './video-dashboard.component.html',
  styleUrls: ['./video-dashboard.component.css']
})
export class VideoDashboardComponent implements OnInit, OnDestroy {
  videoList: Observable<Video[]>;
  destroy = new Subject();

  constructor(vls: VideoLoaderService) {
    this.videoList = vls.filteredVideos;

    this.videoList.pipe(takeUntil(this.destroy))
      .subscribe(list => console.log(list));
  }

  ngOnInit() {
  }

  ngOnDestroy() {
    this.destroy.next();
  }

}

import { Component, OnInit } from '@angular/core';
import { DomSanitizer, SafeUrl } from '@angular/platform-browser';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

import { VideoLoaderService } from '../../video-loader.service';
import { Video } from '../../types';

@Component({
  selector: 'app-video-player',
  templateUrl: './video-player.component.html',
  styleUrls: ['./video-player.component.css']
})
export class VideoPlayerComponent implements OnInit {
  videoUrl: Observable<SafeUrl>;
  urlPrefix = 'https://www.youtube.com/embed/';

  constructor(vls: VideoLoaderService, sanitizer: DomSanitizer) {
    this.videoUrl = vls.selectedVideo.pipe(
      map(video => sanitizer.bypassSecurityTrustResourceUrl(this.urlPrefix + video.id))
    );
  }

  ngOnInit() {
  }

}

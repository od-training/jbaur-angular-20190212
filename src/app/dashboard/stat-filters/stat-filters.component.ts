import { Component, OnDestroy } from '@angular/core';
import { FormGroup, FormBuilder } from '@angular/forms';
import { Subject } from 'rxjs';

import { VideoLoaderService } from '../../video-loader.service';
import { takeUntil } from 'rxjs/operators';

@Component({
  selector: 'app-stat-filters',
  templateUrl: './stat-filters.component.html',
  styleUrls: ['./stat-filters.component.css']
})
export class StatFiltersComponent implements OnDestroy {
  filters: FormGroup;
  destroy = new Subject();

  constructor(fb: FormBuilder, vls: VideoLoaderService) {
    this.filters = fb.group({
      title: [''],
      author: ['']
    });

    this.filters.valueChanges.pipe(takeUntil(this.destroy))
      .subscribe(value => vls.filters.next(value));
  }

  ngOnDestroy() {
    this.destroy.next();
  }

}

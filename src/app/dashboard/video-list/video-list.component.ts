import { Component, OnInit, Input } from '@angular/core';
import { Observable } from 'rxjs';

import { Video } from '../../types';
import { VideoLoaderService } from '../../video-loader.service';

@Component({
  selector: 'app-video-list',
  templateUrl: './video-list.component.html',
  styleUrls: ['./video-list.component.css']
})
export class VideoListComponent implements OnInit {
  @Input() videos: Video[] = [];
  selectedVideoId: Observable<string>;

  constructor(vls: VideoLoaderService) {
    this.selectedVideoId = vls.selectedVideoId;
  }

  ngOnInit() {
  }

}

export interface Video {
  title: string;
  author: string;
  id: string;
  viewDetails: VideoViewDetails[];
}

interface VideoViewDetails {
  age: number;
  region: string;
  date: string;
}

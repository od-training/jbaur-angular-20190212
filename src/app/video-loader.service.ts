import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ActivatedRoute, Params } from '@angular/router';
import { Store } from '@ngrx/store';
import { Observable, BehaviorSubject, combineLatest } from 'rxjs';
import { map, pluck, switchMap, filter, take } from 'rxjs/operators';

import { Video } from './types';
import { AppState, GotVideoList } from './state';

const apiUrl = 'https://api.angularbootcamp.com/videos';

@Injectable({
  providedIn: 'root'
})
export class VideoLoaderService {
  selectedVideoId: Observable<string>;
  selectedVideo: Observable<Video>;
  filters = new BehaviorSubject<Partial<Video>>({});
  filteredVideos: Observable<Video[]>;

  constructor(
    private http: HttpClient,
    route: ActivatedRoute,
    store: Store<AppState>
  ) {
    this.selectedVideoId = route.queryParams.pipe(
      pluck<Params, string>('videoId')
    );

    this.selectedVideo = this.selectedVideoId.pipe(
      filter(id => !!id),
      switchMap(id => this.getVideo(id))
    );

    this.getVideos().pipe(take(1))
      .subscribe(videos => store.dispatch(new GotVideoList(videos)));

    this.filteredVideos = combineLatest(this.getVideos(), this.filters).pipe(
      map(([videoList, filters]) => {
        return videoList.filter(video => {
          return (!filters.title || (video.title.toUpperCase().indexOf(filters.title.toUpperCase()) > -1)) &&
            (!filters.author || (video.author.toUpperCase().indexOf(filters.author.toUpperCase()) > -1));
        });
      })
    );
  }

  private getVideos(): Observable<Video[]> {
    return this.http.get<Video[]>(apiUrl).pipe(
      map(videos => {
        videos.forEach(vid => vid.title = vid.title.toUpperCase());
        return videos;
      })
    );
  }

  getVideo(videoId: string): Observable<Video> {
    return this.http.get<Video>(`${apiUrl}/${videoId}`);
  }
}

import { BrowserModule } from '@angular/platform-browser';
import { NgModule, LOCALE_ID } from '@angular/core';
import { registerLocaleData } from '@angular/common';
import localeIs from '@angular/common/locales/is';
import { HttpClientModule } from '@angular/common/http';
import { StoreModule } from '@ngrx/store';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { AppState, videoList } from './state';

registerLocaleData(localeIs);

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    StoreModule.forRoot<AppState>({ videoList })
  ],
  providers: [ { provide: LOCALE_ID, useValue: 'is' } ],
  bootstrap: [AppComponent]
})
export class AppModule { }

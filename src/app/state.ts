import { Action } from '@ngrx/store';

import { Video } from './types';

export interface AppState {
  videoList: Video[];
}

const gotVideoList = 'GOT_VIDEO_LIST';
export class GotVideoList implements Action {
  type = gotVideoList;
  constructor(public videos: Video[]) { }
}

export function videoList(state: Video[] = [], action: Action): Video[] {
  switch (action.type) {
    case gotVideoList:
      return (action as GotVideoList).videos;

    default:
      return state;
  }
}
